<?php $permisos = $this->session->userdata('permisos'); ?>
<!-- Navbar Start -->
<nav class="navigation">
    <ul class="list-unstyled">
        <li class=""><a href="<?=base_url('cms/main')?>"><i class="zmdi zmdi-view-dashboard"></i> <span class="nav-label">Inicio</span></a></li>
        <?php if(isGranted('usuarios')){ ?>
        <li class="has-submenu <?=($current == 'usuarios')?'active':''?>">
            <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Usuarios</span><span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?=base_url('cms/usuarios')?>">Usuarios</a></li>
                <li><a href="<?=base_url('cms/usuarios')?>">Roles</a></li>
            </ul>
        </li>
       
        <?php if(isGranted('opciones')) { ?>
            <li class="has-submenu <?=($current == 'opciones')?'active':''?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Gestion de la Configuracion</span><span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    <li><a href="<?=base_url('cms/modulos')?>"></a></li>
                    <li><a href="<?=base_url('cms/opciones')?>"></a></li>
                </ul>
            </li>
        <? } ?>
    </ul>
</nav>
